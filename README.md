> ## Just another Tutorial about Biopython

**Biopython** is a set of free Python tools for Computational Molecular Biology and Bioinformatics.

https://biopython.org/

## Installation

```python
# Install Bipython
> pip install biopython

# Check version
> import bio
> print(Bio.__version__)
1.78

```

## Tutorial through Jupyter Notebooks

1. Getting Started with Sequences (**Seq.ipynb**)

    * Sequence objects (Seq)
    * Central Dogma of Molecular Biology
        - Transcription
        - Translation
    * Genetic Code
    * MutableSeq objects   

2. Annotated Sequences (**SeqRecord.ipynb**)

    * Sequence annotation objects (SeqRecord)
    * SeqRecord from FASTA file (SARS-CoV-2 genome: 'NC_045512.fasta')
    * SeqRecord from GenBank file (SARS-CoV-2 genome: 'NC_045512.gb')
        - Annotations
        - Features

3. Sequence Features (**SeqFeature.ipynb**)

    * Working with Features (SeqFeature)
        - Using features information editing GenBank files
        - E gene features (SARS-CoV-2) from GenBank file
    * Genes COVID-19
    * Create dictionary for COVID-19 genes and save it as pickle

4. Sequence Input/Output (**SeqIO**)
    * Create a multiple FASTA file
    * Parse FASTA
    * Parse GenBank
    * Download from NCBI
    * GenBank sequence files as dictionaries
    * Writing sequence files
    * Convert format
    * FASTA file with genes created from genome (SARS-CoV-2)












